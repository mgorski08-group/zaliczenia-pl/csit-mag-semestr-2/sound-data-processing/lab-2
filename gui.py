import threading
import time

import numpy as np
from ctypes import c_void_p
from OpenGL.GL import *
import glfw
import random

width = 1300
height = 250


def init(data, x_off, key_callback):
    # Initialize the library
    if not glfw.init():
        return

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(width, height, "Synth", None, None)
    # glfw.get_primary_monitor()
    if not window:
        glfw.terminate()
        return

    # Make the window's context current
    glfw.make_context_current(window)
    glfw.swap_interval(1)

    def resizecb(win, w, h):
        global width
        global height
        width = w
        height = h
        glViewport(0, 0, w, h)

    glfw.set_key_callback(window, key_callback)
    glfw.set_framebuffer_size_callback(window, resizecb)

    program = glCreateProgram()
    vertID = glCreateShader(GL_VERTEX_SHADER)
    fragID = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(vertID, '''
    #version 330

uniform int width;
uniform int height;

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 xy;
layout (location = 2) in vec2 loc;

out vec2 xy_out;

void main() {
    vec2 new_pos;
    new_pos.x = pos.x*10.0/width;
    new_pos.y = pos.y*10.0/height;
    gl_Position = vec4(new_pos+loc, 0, 1);
    xy_out = xy;
}

    ''')
    glShaderSource(fragID, '''
    #version 330

in vec2 xy_out;

void main() {
    float r = sqrt(xy_out.x*xy_out.x+xy_out.y*xy_out.y);
    if (r < 0.3) {
        gl_FragColor = vec4(0.7, 0.5, 0.1, 1);
        return;
    }
    if (r < 1) {
        float f = 1.4285714285-r*1.4285714285;
        gl_FragColor = vec4(0.7, 0.5, 0.1, f);
        return;
    }
    discard;

}

    ''')

    glCompileShader(vertID)
    if glGetShaderiv(vertID, GL_COMPILE_STATUS) == GL_FALSE:
        print("Failed to compile vertex shader!")
        print(glGetShaderInfoLog(vertID))
        return

    glCompileShader(fragID)
    if glGetShaderiv(fragID, GL_COMPILE_STATUS) == GL_FALSE:
        print("Failed to compile fragment shader!")
        print(glGetShaderInfoLog(fragID))
        return

    glAttachShader(program, vertID)
    glAttachShader(program, fragID)
    glLinkProgram(program)
    glValidateProgram(program)

    clear_program = glCreateProgram()
    clear_vertID = glCreateShader(GL_VERTEX_SHADER)
    clear_fragID = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(clear_vertID, '''
    #version 330
    layout (location = 0) in vec2 pos;

    void main() {
        gl_Position = vec4(pos, 0, 1);
    }

        ''')
    glShaderSource(clear_fragID, '''
    #version 330
    in vec2 xy_out;

    void main() {
        gl_FragColor = vec4(0, 0, 0, 0.1);
    }

        ''')

    glCompileShader(clear_vertID)
    if glGetShaderiv(clear_vertID, GL_COMPILE_STATUS) == GL_FALSE:
        print("Failed to compile vertex shader!")
        print(glGetShaderInfoLog(clear_vertID))
        return

    glCompileShader(clear_fragID)
    if glGetShaderiv(clear_fragID, GL_COMPILE_STATUS) == GL_FALSE:
        print("Failed to compile fragment shader!")
        print(glGetShaderInfoLog(clear_fragID))
        return

    glAttachShader(clear_program, clear_vertID)
    glAttachShader(clear_program, clear_fragID)
    glLinkProgram(clear_program)
    glValidateProgram(clear_program)

    VAO = glGenVertexArrays(1)
    glBindVertexArray(VAO)

    IBO = glGenBuffers(1)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, np.array([0, 1, 2, 2, 3, 0], dtype=np.int32), GL_STATIC_DRAW)

    VBO = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, VBO)
    glBufferData(GL_ARRAY_BUFFER, np.array([-1, -1,
                                            1, -1,
                                            1, 1,
                                            -1, 1], dtype=np.float32), GL_STATIC_DRAW)

    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0, 2, GL_FLOAT, False, 0, c_void_p(0))

    xy_vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, xy_vbo)
    glBufferData(GL_ARRAY_BUFFER, np.array([-1, -1,
                                            1, -1,
                                            1, 1,
                                            -1, 1], dtype=np.float32), GL_STATIC_DRAW)

    glEnableVertexAttribArray(1)
    glVertexAttribPointer(1, 2, GL_FLOAT, False, 0, c_void_p(0))

    loc_vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, loc_vbo)
    glBufferData(GL_ARRAY_BUFFER, np.array([-0.5, -0.5,
                                            0, 0,
                                            0, 0.5,
                                            0.6, 0.2], dtype=np.float32), GL_STATIC_DRAW)

    glEnableVertexAttribArray(2)
    glVertexAttribPointer(2, 2, GL_FLOAT, False, 0, c_void_p(0))
    glVertexAttribDivisor(2, 1)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)

    clear_VAO = glGenVertexArrays(1)
    glBindVertexArray(clear_VAO)

    clear_VBO = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, clear_VBO)
    glBufferData(GL_ARRAY_BUFFER, np.array([-1, -1,
                                            1, -1,
                                            1, 1,
                                            -1, 1], dtype=np.float32), GL_STATIC_DRAW)

    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0, 2, GL_FLOAT, False, 0, c_void_p(0))

    clear_IBO = glGenBuffers(1)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, clear_IBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, np.array([0, 1, 2, 2, 3, 0], dtype=np.int32), GL_STATIC_DRAW)

    coords = np.empty(len(data) * 2, dtype=np.float32)

    w_uniform = glGetUniformLocation(program, "width")
    h_uniform = glGetUniformLocation(program, "height")

    glDrawBuffer(GL_FRONT)

    # Loop until the user closes the window
    while not glfw.window_should_close(window):
        time.sleep(1/60)
        glUseProgram(clear_program)
        glBindVertexArray(clear_VAO)
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, c_void_p(0))
        # glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # Render here, e.g. using pyOpenGL

        glUseProgram(program)
        glBindVertexArray(VAO)

        glUniform1i(w_uniform, width)
        glUniform1i(h_uniform, height)

        offset = x_off[0] / (len(data) - 1)  # ???
        i = 0
        for x, y in zip(np.linspace(-1 + offset, 1 + offset, len(data)), data):
            coords[i] = x
            coords[i + 1] = y
            i += 2

        glBindBuffer(GL_ARRAY_BUFFER, loc_vbo)
        glBufferData(GL_ARRAY_BUFFER, coords, GL_STATIC_DRAW)

        # showScreen()
        glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, c_void_p(0), len(coords) // 2)

        # Swap front and back buffers
        # glfw.swap_buffers(window)
        glFinish()

        # Poll for and process events
        glfw.poll_events()

    glfw.terminate()
