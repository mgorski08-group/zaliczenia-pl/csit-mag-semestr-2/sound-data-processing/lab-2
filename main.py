from collections import deque

import glfw
import numpy as np
import sounddevice as sd

import gen
import gui

buf = deque(np.zeros(500), maxlen=500)


def trigger_fun(window):
    # Rising edge
    return window[0] < 0 < window[1]


class Scope:
    def __init__(self, view_buff_size, mem_depth):
        self.view_buff_size = view_buff_size
        self.mem_depth = mem_depth
        self.x_off = np.zeros(1)

        self.memory = deque(np.zeros(mem_depth), maxlen=mem_depth)
        self.view_buff = np.zeros(view_buff_size, dtype=np.float32)
        self.delay = 0

    def input_data(self, data):
        for d in data:
            self.memory.append(d)
            self.delay += 1
            if self.delay < 10:
                continue
            self.delay = 0
            if trigger_fun((self.memory[-2], self.memory[-1])):
                for i in range(self.view_buff_size):
                    self.view_buff[i] = self.memory[i]
                self.x_off[0] = self.memory[-1] / (self.memory[-1] - self.memory[-2])


def key_to_freq(key):
    base_a = 440
    return {
        glfw.KEY_A: base_a * (2 ** (-1 / 12)),  # Gis
        glfw.KEY_Z: base_a * (2 ** (0 / 12)),  # A
        glfw.KEY_S: base_a * (2 ** (1 / 12)),  # Ais
        glfw.KEY_X: base_a * (2 ** (2 / 12)),  # H
        glfw.KEY_C: base_a * (2 ** (3 / 12)),  # C
        glfw.KEY_F: base_a * (2 ** (4 / 12)),  # Cis
        glfw.KEY_V: base_a * (2 ** (5 / 12)),  # D
        glfw.KEY_G: base_a * (2 ** (6 / 12)),  # Dis
        glfw.KEY_B: base_a * (2 ** (7 / 12)),  # E
        glfw.KEY_N: base_a * (2 ** (8 / 12)),  # F
        glfw.KEY_J: base_a * (2 ** (9 / 12)),  # Fis
        glfw.KEY_M: base_a * (2 ** (10 / 12)),  # G
        glfw.KEY_K: base_a * (2 ** (11 / 12)),  # Gis
        glfw.KEY_COMMA: base_a * (2 ** (12 / 12)),  # A
        glfw.KEY_L: base_a * (2 ** (13 / 12)),  # Ais
        glfw.KEY_PERIOD: base_a * (2 ** (14 / 12)),  # H
        glfw.KEY_SLASH: base_a * (2 ** (15 / 12)),  # C
        glfw.KEY_APOSTROPHE: base_a * (2 ** (16 / 12)),  # Cis
    }[key]


note_state = {}

sc = Scope(500, 500)

on = False

freq = 0


def key_callback(window, key, scancode, action, mods):
    global on
    global freq
    try:
        freq = key_to_freq(key)
        if action == glfw.PRESS:
            on = True
        elif action == glfw.RELEASE:
            on = False
    except Exception:
        pass


ph = 0
state = 0


def envelope(init_state, frames):
    if init_state[0] == "r":
        e = np.linspace(init_state[1], init_state[1] + frames / 1000, frames)
        return np.fmin(e, 1)
    elif init_state[0] == "f":
        e = np.linspace(init_state[1], init_state[1] - frames / 20000, frames)
        return np.fmax(e, 0)


def cb(outdata, frames, a, b):
    # import pydevd
    # pydevd.settrace(suspend=False)
    global ph
    global state
    global buf
    global sc
    global freq
    env = envelope(("r" if on else "f", state), frames)
    state = env[-1]

    d, p = gen.mixed(44100, freq, env * 0.8, frames / 44100, ph)
    ph = p
    outdata[:] = np.reshape(d, (-1, 1))

    sc.input_data(outdata[:, 0])


def main():
    with sd.OutputStream(channels=1, samplerate=44100, dtype="float32", callback=cb):
        gui.init(sc.view_buff, sc.x_off, key_callback)


if __name__ == '__main__':
    main()
