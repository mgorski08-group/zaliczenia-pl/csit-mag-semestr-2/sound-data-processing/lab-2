import numpy as np


def window(arr, window_size, stride=1):
    for i in range(0, len(arr) - window_size + 1, stride):
        yield arr[i:i + window_size]


def trans(func, sample_rate, freq, envelope, time, phase):
    x = np.linspace(0, freq * time * 2 * np.pi, int(sample_rate * time))
    end_phase = ((freq * time) % 1) * 2 * np.pi + phase
    while end_phase > 2 * np.pi:
        end_phase -= 2 * np.pi
    return func(x + phase) * envelope, end_phase


def sin(sample_rate, freq, envelope, time, phase=0):
    return trans(np.sin, sample_rate, freq, envelope, time, phase)


def triangle(sample_rate, freq, envelope, time, phase=0):
    def f(x):
        x = (x + np.pi / 2) % (2 * np.pi) - np.pi / 2
        return np.where((np.pi / 2 < x) & (x <= 3 * np.pi / 2), 2 - x / (np.pi / 2), x / (np.pi / 2))

    return trans(f, sample_rate, freq, envelope, time, phase)


def sawtooth(sample_rate, freq, envelope, time, phase=0):
    def f(x):
        x = (x + np.pi) % (2 * np.pi) - np.pi
        return x / np.pi

    return trans(f, sample_rate, freq, envelope, time, phase)


def rectangular(sample_rate, freq, envelope, time, phase=0):
    def f(x):
        x = (x + np.pi) % (2 * np.pi) - np.pi
        return np.where(x < 0, -1, 1)

    return trans(f, sample_rate, freq, envelope, time, phase)


def white_noise(sample_rate, freq, envelope, time, phase=0):
    def f(x):
        return np.random.uniform(-1, 1, len(x))

    return trans(f, sample_rate, freq, envelope, time, phase)


def mixed(sample_rate, freq, envelope, time, phase=0):
    s, ph = sin(sample_rate, freq, envelope, time, phase)
    tri = triangle(sample_rate, freq, envelope, time, phase)[0]
    saw = sawtooth(sample_rate, freq, envelope, time, phase)[0]
    rect = rectangular(sample_rate, freq, envelope, time, phase)[0]

    return s * 0.1 + tri * 0.5 + saw * 0.3 + rect * 0.1, ph


def red_noise(sample_rate, freq, envelope, time, phase=0):
    if freq == 0:
        freq = sample_rate
    a = int(sample_rate / freq)
    white_n = np.random.uniform(-1, 1, int(sample_rate * time) + a)

    avgs = [np.average(w) for w in window(white_n, a + 1, 1)]
    red_n = np.array(avgs) * envelope
    return red_n, 0
